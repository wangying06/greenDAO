/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import AbilityStage from "@ohos.application.AbilityStage"
import { ExampleOpenHelper } from '../pages/ExampleOpenHelper';
import { DaoMaster } from '@ohos/greendao';
import { Database } from '@ohos/greendao';
import { Note } from '../pages/Note';
import { Migration } from '@ohos/greendao'
import { ColumnType } from '@ohos/greendao'

export default class MyAbilityStage extends AbilityStage {
    async onCreate() {
        let newVersion = 2;
        globalThis.contt = this.context;
        globalThis.entityCls = {}
        // regular SQLite database
        let helper: ExampleOpenHelper = new ExampleOpenHelper(this.context, "notes.db");
        //设定数据加密密钥，加密后不可变更，加密和非加密库暂不能切换（普通数据库不能在设定为加密库，加密库不能变更为普通库，一经生成不可变更）
        //todo 注：参数必须为非空，否则会认定为非加密库（如：''或者""都为非加密库）
        helper.setEncryptKeyStr('encryptKey');
        let db: Database = await helper.getWritableDb();
        //将所有的表(新增,修改,已存在)加到全局
        helper.setEntitys(Note);
        //调用创建表方法,将新增表创建,若无新增则不创建表
        helper.onCreate_D(db);

        //todo 数据库新增列示例
        //Migration为表更新实例,也可调用Migration.backupDB对当前数据库进行备份
        let migration = new Migration("notes.db", "NOTE", newVersion).addColumn("MONEYS", ColumnType.realValue);
        //将所有表更新实例放到ExampleOpenHelper的父级中
        helper.setMigration(migration);
        //设置新的数据库版本,如果新版本中包含表更新实例将在这调用onUpgrade_D()进行表更新
        helper.setVersion(newVersion)
        globalThis.daoSession = new DaoMaster(db).newSession();
    }
}